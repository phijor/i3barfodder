from i3bfutils import cache
from time import sleep


# Test normal functions
i = 0
@cache.timeout(seconds=0.1)
def increment_i():
    global i
    i += 1

def test_timeout_normal_function():
    global i
    assert i == 0
    increment_i()
    assert i == 1
    increment_i()
    increment_i()
    increment_i()
    assert i == 1
    sleep(0.2)
    increment_i()
    increment_i()
    increment_i()
    assert i == 2


j = 0
@cache.timeout(each='second')
def increment_j():
    global j
    j += 1

def test_timeout_normal_function_each_second():
    global j
    assert j == 0
    increment_j()
    assert j == 1
    increment_j()
    increment_j()
    increment_j()
    assert j == 1
    sleep(1)
    increment_j()
    increment_j()
    increment_j()
    assert j == 2


# Test bound functions
class Incrementer():
    def __init__(self):
        self.i = 0

    @cache.timeout(seconds=0.1)
    def increment(self):
        self.i += 1

def test_timeout_bound_function():
    inc = Incrementer()
    assert inc.i == 0
    inc.increment()
    inc.increment()
    inc.increment()
    assert inc.i == 1
    sleep(0.2)
    inc.increment()
    inc.increment()
    inc.increment()
    assert inc.i == 2
