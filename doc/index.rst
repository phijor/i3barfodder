.. This file is only needed for readthedocs.org.

i3barfodder documentation
=========================

.. include:: fragments/i3bf-description.txt

Contents
========

* :doc:`config.example`

Manual pages
------------

* :doc:`i3bf <man/i3bf>`
* :doc:`i3bf-cpu <man/i3bf-cpu>`
* :doc:`i3bf-mail <man/i3bf-mail>`
* :doc:`i3bf-net <man/i3bf-net>`
* :doc:`i3bf-pkgmgr <man/i3bf-pkgmgr>`
* :doc:`i3bf-storage <man/i3bf-storage>`
* :doc:`i3bf-temperature <man/i3bf-temperature>`
* :doc:`i3bf-timedate <man/i3bf-timedate>`

i3bfutils
---------

The following modules are available for workers written in Python 3:

* :doc:`i3bfutils/cache`
* :doc:`i3bfutils/convert`
* :doc:`i3bfutils/cpu`
* :doc:`i3bfutils/dates`
* :doc:`i3bfutils/delay`
* :doc:`i3bfutils/inotify`
* :doc:`i3bfutils/io`
* :doc:`i3bfutils/mail`
* :doc:`i3bfutils/network`
* :doc:`i3bfutils/pkgmgr`
* :doc:`i3bfutils/rate`
* :doc:`i3bfutils/sensors`
* :doc:`i3bfutils/storage`
* :doc:`i3bfutils/template`
