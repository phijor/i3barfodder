NAME
====

i3bf-pkgmgr - Package update notifier


DESCRIPTION
===========

A worker for the i3bar status line generator **i3barfodder** that displays the
number of new software packages for your distribution.  Clicking on the
notification removes it until the number of packages changes again.


CONFIGURATION
=============

.. include:: ../fragments/worker-configuration.rst


ENVIRONMENT VARIABLES
---------------------

**PKGMGR** = *manager*  (must be set)
    The name of the package manager of your distribution.

    Supported package managers are:

    +-----------+-------------------------------------------------------------+
    | *manager* | Compatible distribution                                     |
    +===========+=============================================================+
    | *apt*     | Debian and its derivatives (e.g. Ubuntu, Mint, etc)         |
    +-----------+-------------------------------------------------------------+

**LAYOUT** = *string*  (default: see below)
    .. include:: ../fragments/worker-layout-short.rst

    The default layout is::

        ({new} update(s?{_new}!=1) available?{_new}>0)

    This defines a block that displays "{new} update(s) available", appending
    the "s" to "update" if necessary.


PLACEHOLDERS
------------

**{new}**
    The number of new software packages ready to be installed.


LAYOUT
------

.. include:: ../fragments/worker-layout-long.rst


SEE ALSO
========

i3bf(1)
