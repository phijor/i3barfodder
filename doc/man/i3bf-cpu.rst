NAME
====

i3bf-cpu - CPU load per core or overall


DESCRIPTION
===========

A worker for the i3bar status line generator **i3barfodder** that displays
CPU usage per core or in total.


CONFIGURATION
=============

.. include:: ../fragments/worker-configuration.rst


ENVIRONMENT VARIABLES
---------------------

**INTERVAL** = *number*  (default: 1)
   Seconds between updates.

**SAMPLES** = *integer*  (default: 2)
   Number of previous usage rates to compute the average.  Increasing this
   value makes it less jumpy, decreasing makes it more responsive.

**PERCORE** = *boolean*  (default: False)
   Whether to display LAYOUT for each CPU core or just once for combined usage.

**LAYOUT** = *string*  (default: see below)
   .. include:: ../fragments/worker-layout-short.rst

   The default layout is::

       [
           <small>({percent}?={_percent}>DISPLAY_MIN) </small>
           vbar({_percent}, scale=SCALE)
           |{_percent}:COLORS
       ]3

   This defines a block that shows CPU usage in percent if it is larger than
   **DISPLAY_MIN**, followed by CPU usage as a vertical bar.  Both are colored
   dynamically with the colors specified in **COLORS**.  The 3 after the block
   specifies "separator_block_width", i.e. the gap between blocks.


DEFAULT LAYOUT VARIABLES
^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../fragments/worker-layout-default-variables.rst

**DISPLAY_MIN** = *number*  (default: 10%)
    The minimum CPU usage in percent to display it as a percentage instead of as a
    bar only.

**COLORS** = *colorlist*  (default: #568-#6BF)
    The list of colors that are used to dynamically change display color depending
    on CPU usage.

**SCALE** = *scale*  (default: linear)
    How much emphasis to put on smaller values.  Must be "linear", "sqrt" or
    "log".


PLACEHOLDERS
------------

**{percent}**
    Current CPU or CPU core usage, depending on whether **PERCORE** is enabled
    or not.

**{cores}**
    The number of available CPU cores.

**{core}**
    The core number, starting with 1.  This is only available if **PERCORE** is
    enabled.


LAYOUT
------

.. include:: ../fragments/worker-layout-long.rst


SEE ALSO
========

i3bf(1)
