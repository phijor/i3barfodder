NAME
====

i3bf-storage - Usage, IO rates, etc of mounted partitions


DESCRIPTION
===========

A worker for the i3bar status line generator **i3barfodder** that displays
information about mountpoints.


CONFIGURATION
=============

.. include:: ../fragments/worker-configuration.rst


ENVIRONMENT VARIABLES
---------------------

**WHITELIST** = *list*  (default: <unset>)
    Comma-separated list of mountpoints to display.  Unlisted mountpoints are
    never displayed.

    If **WHITELIST** is set, **BLACKLIST** is ignored.

    An asterisk ("\*") at the end of a path includes all mountpoints that
    start with that path; e.g. "/media/\*" includes all mountpoints that start
    with "/media/".

**BLACKLIST** = *list*  (default: <unset>)
    Comma-separated list of mountpoints to ignore.  Unlisted mountpoints are
    displayed if mounted.  Globbing works as for **WHITELIST**.

**PERMALIST** = *list*  (default: <unset>)
    Comma-separated list of mountpoints to display even if they're not
    mounted.  There is no globbing for **PERMALIST** items.

**CLICK[1|2|3|...]** = *shell command*  (default: <unset>)
    Command to run when a mountpoint is clicked with button 1, 2, 3, etc.
    Commands can use mountpoint attributes by using placeholders.

    This example unmounts a mountpoint using udiskie when it is clicked with
    the left mouse button::

        CLICK1 = udiskie-umount {path}

**INTERVAL** = *number*  (default: 1)
    Seconds between updates.

**SAMPLES** = *integer*  (default: 3)
    Number of previously measured I/O rates to compute an average.  Increasing
    this value makes the display less jumpy, decreasing makes it more
    responsive.

**LAYOUT** = *string*  (default: see below)
    .. include:: ../fragments/worker-layout-short.rst

    This variable describes how a single mountpoint is displayed.

    The default layout is::

        [● |{_rw}:max=1M:LED_COLORS]
        [{path} |{_mounted}:max=1:PATH_COLORS]
        [<small>({free} free?{_mounted}=1)</small>|
         {_used%}:USED_COLORS:min=USED_MIN:max=USED_MAX]30

    This defines three blocks:

    1. An LED-like unicode character that changes color depending on I/O
       activity (**{_rw}**).  The maximum color is reached at a combined
       read-write rate of 1MB/s.

    2. The mountpoint path that changes color depending on whether
       mountpoint is mounted or not.  See **PERMALIST**.

    3. How much free space is available, but only if mountpoint is
       mounted.  If more than **USED_MIN** percent of the total space is
       used, the color starts to change until **USED_MAX** is reached.
       The gap between this block and the next one
       ("separataor_block_width") is 30 pixels (except for the
       last/rightmost mountpoint; see **separator_block_width** in the
       **LAYOUT** section).


DEFAULT LAYOUT VARIABLES
^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../fragments/worker-layout-default-variables.rst

**LED_COLORS** = *list*  (default: #665-#FF4)
    List of colors that is used to dynamically change color depending on the
    current I/O rate.

**PATH_COLORS** = *color*  (default: #974-#D95)
    The color of the mountpoint.  The first color is used if the mountpoint is
    mounted and the last color is used if it unmounted.

**USED_COLORS** = *list*  (default: #D95-#F51)
    List of colors that is used to dynamically change color depending on the
    currently used space.

**USED_MIN** = *percentage*  (default: 70%)
    The minimum amount of used space in percent before the color starts to
    change from the first color in **USED_COLORS** to the next one.

**USED_MAX** = *percentage*  (default: 95%)
    The amount of used space in percent at which the final color in
    **USED_COLORS** is reached.


PLACEHOLDERS
------------

**{path}**
    Mountpoint path

**{mounted}**
    Mount status of **{path}** (**{path}** is "true" or "false"; **{_path}**
    is "0" or "1")

**{devpath}**
    Device file path

**{devname}**
    Device file name

**{fs}**
    Filesystem name

**{free}**
    Available space

**{used}**
    Used space

**{total}**
    Total amount of space

**{free%}**
    Available space in percent

**{used%}**
    Used space in percent

**{read}**
    Read rate per second

**{write}**
    Write rate per second

**{rw}**
    Combined read-write rate per second



LAYOUT
------

.. include:: ../fragments/worker-layout-long.rst


SEE ALSO
========

i3bf(1)
