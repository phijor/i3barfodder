NAME
====

i3bf-timedate - Time, date and a reminder on certain dates


DESCRIPTION
===========

A worker for the i3bar status line generator **i3barfodder** that displays
the current time, date and a simple reminder for certain dates.


CONFIGURATION
=============

.. include:: ../fragments/worker-configuration.rst


ENVIRONMENT VARIABLES
---------------------

**INTERVAL** = *number*  (default: 1)
    Seconds between updates.

**LAYOUT** = *string*  (default: see below)
    .. include:: ../fragments/worker-layout-short.rst

    The default layout is::

        [%! |_%!:ALARM_COLOR]
        [DATE_FORMAT  |DATE_COLOR]
        [TIME_FORMAT|TIME_COLOR]

    This defines three blocks.  The first block indicates that one of the
    dates in **ALARMDATES** is near or today.  The second and third blocks
    show the current date and time respectively.

**ALARMDATES** = *list*  (default=<empty>)
    A list of dates separated by semicolons (";").  For a few days before each
    date an alarm indicator is displayed in place of the **%!** placeholder.
    See the **DATE FORMATS** section.

**ALARMINDICATORS** = *list*  (default=⚫;⚪;⚬;·)
    A list of indicators separated by semicolons (";").  The first alarm indicator
    replaces the **%!** placeholder on any of the alarm dates, the second indicator
    is used on the day before any of the alarm dates, and so on.


DEFAULT LAYOUT VARIABLES
^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../fragments/worker-layout-default-variables.rst

**DATE_FORMAT** = *string*  (default: %x)
    Specifies the date format.

**TIME_FORMAT** = *string*  (default: %X)
    Specifies the time format.

**DATE_COLOR** = *string*  (default: #9D3)
    Specifies the date color.

**TIME_COLOR** = *string*  (default: #DE3)
    Specifies the time color.


PLACEHOLDERS
------------

**%!**
    One of the **ALARMINDICATORS** reminding you of one of the dates in
    **ALARMDATES**.  The machine-readable format **_%!** is the number of days
    until the next alarm date.

Any of the directives used by the Pyhon 3 function time.strftime() are
placeholders.  They are documented `here
<https://docs.python.org/3/library/time.html#time.strftime>`_.  Most of them
are identical to the FORMAT sequences documented in date(1).


DATE FORMATS
------------

Valid formats for the dates in *ALARMDATES* are:

**YYYY-MM-DD**
    An absolute date that occurs once.

**MM-DD**
    An absolute date that occurs each year.

**<DATE NAME>**
    The name of a date (e.g. "Christmas", "New Year's Day", etc). To get a
    list of known date names run 'i3bf-datetime --list-datenames' or
    'i3bf-datetime -l'.

It is possible to add or subtract days or weeks to any of these date formats
by appending a delta.  Good Friday, for example, is two days before Easter:
"Easter-2d"

The delta format is: `[+-]<NUMBER><UNIT>`

Valid UNITs are "d" for days, "w" for weeks and abbreviated weekdays (Mo/Mon,
Tu/Tue, We/Wed, Th/Thu, Fr/Fri, Sa/Sat, Su/Sun).  Case is ignored.

If UNIT is a weekday, the date is adjusted to the NUMBERth weekday before or
after it.  For example, because Easter is always on a Sunday, Good Friday can
also be specified as "Easter-1fri", and the Friday before Good Friday would be
"Easter-2fri".

You can also apply multiple deltas: "Easter-2fri+1w" still resolves to Good
Friday, and "Easter-2fri+1w+1sun" is identical to "Easter".


LAYOUT
------

.. include:: ../fragments/worker-layout-long.rst


SEE ALSO
========

i3bf(1)
