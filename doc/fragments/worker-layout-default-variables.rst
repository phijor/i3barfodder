The default layout uses the following environment variables to allow some
configuration without having to write complex layout strings.

.. note::
   If you set the **LAYOUT** variable, these variables have no effect.
