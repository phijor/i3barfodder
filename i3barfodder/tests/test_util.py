import unittest
from i3barfodder import util
from i3barfodder import error
import random

class TestRatio(unittest.TestCase):
    def test_defaults(self):
        r = util.Ratio()
        self.assertEqual(float(r), 0)  # float() returns number between 0 and 1
        for (value,ratio) in ((10, 1), (20, 1), (50, 1),      # Increase max
                              (25, 0.5), (1, 0.02), (50, 1),  # Test new max
                              (0, 0), (-10, 0), (-50, 0),     # Decrease min
                              (0, 0.5), (50, 1)):             # Test new min
            r.update(('value',value))
            self.assertEqual(float(r), ratio)

    def test_soft_min_max(self):
        r = util.Ratio(('softmin',-1000), ('softmax',-900))
        self.assertEqual(r['value'], -1000)  # Initial value is min if unspecified
        for (value,ratio) in ((-1000, 0), (-950, 0.5), (-900, 1),   # Test current range
                              (-850, 1), (-800, 1),                 # Increase max
                              (-1000, 0), (-900, 0.5), (-800, 1),   # Test new max
                              (-1100, 0), (-1200, 0),               # Decrease min
                              (-1000, 0.5), (-900, 0.75), (-1100, 0.25)):  # Test new min
            r.update(('value',value))
            self.assertEqual(float(r), ratio)

    def test_hard_min_max(self):
        r = util.Ratio(('min',25), ('max',75))
        self.assertEqual(r.as_percent, 0)
        for (value,ratio) in ((50, 50), (75, 100), (750, 100), (75, 100),
                              (30, 10), (26, 2), (25, 0), (-19, 0), (25, 0)):
            r.update(('value',value))
            self.assertEqual(r.as_percent, ratio)

    def test_switching_hard_soft(self):
        def test_softmax():
            for (value,ratio) in ((MIN, 0), (MIN+(MAX-MIN)/2, 50), (MAX, 100),
                                  (MAX+(MAX-MIN), 100), (MAX, 50)):
                r['value'] = value
                self.assertEqual(r.as_percent, ratio)
            return r['max']

        def test_hardmax():
            for (value,ratio) in ((MIN, 0), (MIN+(MAX-MIN)/2, 50), (MAX, 100),
                                  (MAX+(MAX-MIN), 100), (MAX, 100)):
                r['value'] = value
                self.assertEqual(r.as_percent, ratio)

        def test_softmin():
            for (value,ratio) in ((MIN, 0), (MIN+(MAX-MIN)/2, 50), (MAX, 100),
                                  (MIN-(MAX-MIN), 0), (MIN, 50)):
                r['value'] = value
                self.assertEqual(r.as_percent, ratio)
            return r['min']

        def test_hardmin():
            for (value,ratio) in ((MIN, 0), (MIN+(MAX-MIN)/2, 50), (MAX, 100),
                                  (MIN-(MAX-MIN), 0), (MIN, 0)):
                r['value'] = value
                self.assertEqual(r.as_percent, ratio)

        MIN = 0
        MAX = 100
        r = util.Ratio(('min',MIN), ('softmax',MAX))
        test_hardmin()
        MAX = test_softmax()
        r['max'] = MAX
        test_hardmin()
        test_hardmax()
        r['softmax'] = MAX
        test_hardmin()
        MAX = test_softmax()

        MIN = -1000
        MAX = -500
        r['softmin'] = MIN
        r['max'] = MAX
        MIN = test_softmin()
        test_hardmax()
        r['min'] = MIN
        test_hardmin()
        test_hardmax()

    def test_scale_sqrt(self):
        r = util.Ratio(('min',0), ('max',100), ('scale', 'sqrt'))
        for (value,ratio) in ((0, 0), (25, 50), (64, 80), (100, 100)):
            r.update(('value',value))
            self.assertEqual(r.as_percent, ratio)

    def test_scale_log(self):
        r = util.Ratio(('min',0), ('max',100), ('scale', 'log'))
        for (value,ratio) in ((0, 0), (10, 0.5), (100, 1)):
            r.update(('value',value))
            self.assertEqual(float(r), ratio)

    def test_copy_and_equality(self):
        p1 = util.Ratio(('min',400), ('softmax',500), ('scale', 'log'))
        p1.update(('value',410))
        p2 = p1.copy()
        self.assertEqual(p1, p2)
        p2.update(('value',550))
        self.assertNotEqual(p1, p2)


class Test_str2num(unittest.TestCase):
    def test_int(self):
        self.assertEqual(util.str2num('5'), 5)
        self.assertEqual(util.str2num('5.0'), 5)
        self.assertEqual(util.str2num('5.0001'), 5.0001)

    def test_float(self):
        self.assertEqual(util.str2num('5.0001'), 5.0001)

    def test_unit_prefixes(self):
        for prefix,prefix_num in util._UNIT_PREFIXES:
            for i in range(2000):
                i = i * random.uniform(1, 10)
                self.assertEqual(util.str2num('{}{}'.format(i, prefix)), i*prefix_num)
                self.assertEqual(util.str2num('{} {}'.format(i, prefix)), i*prefix_num)

    def test_strict(self):
        with self.assertRaisesRegex(error.InvalidValue, 'twelve potatoes'):
            util.str2num('twelve potatoes', strict=True)
        self.assertEqual(util.str2num('twelve potatoes', strict=False),
                                      'twelve potatoes')
        with self.assertRaisesRegex(error.InvalidValue, "'no ' is not a number"):
            util.str2num('no % of anything', strict=True)
        self.assertEqual(util.str2num('no % of anything', strict=False),
                                      'no % of anything')

    def test_percent_operator(self):
        self.assertEqual(util.str2num('20%1'), 0.2)
        self.assertEqual(util.str2num('20%1k'), 200)
        self.assertEqual(util.str2num('0.1 % 1 M'), 1000)

    def test_infinity(self):
        self.assertEqual(util.str2num('inf'), float('inf'))


class Test_parse_params(unittest.TestCase):
    def test_string_values(self):
        self.assertEqual(util.parse_params('x=hello'),
                         ((), (('x','hello'),)))
        self.assertEqual(util.parse_params('x=hello:y=goodbye'),
                         ((), (('x','hello'), ('y','goodbye'))))

    def test_numeric_values(self):
        self.assertEqual(util.parse_params('x=1'),
                         ((), (('x',1),)))
        self.assertEqual(util.parse_params('x=17.0:y=17.01'),
                         ((), (('x',17), ('y',17.01))))

    def test_positional_parameters(self):
        self.assertEqual(util.parse_params('abc:x=hello:def:ghi:y=2.5:jkl'),
                         (('abc', 'def', 'ghi', 'jkl'),
                          (('x','hello'), ('y',2.5))))


class Test_parse_args(unittest.TestCase):
    def test_positional_args(self):
        self.assertEqual(util.parse_args('1'), ((1,), ()))
        self.assertEqual(util.parse_args('1, 2'), ((1, 2), ()))
        self.assertEqual(util.parse_args('1, 2,hello'), ((1, 2, 'hello'), ()))

    def test_keyword_args(self):
        self.assertEqual(util.parse_args('one=1'),
                         ((), (('one',1),)))
        self.assertEqual(util.parse_args('one=1, two=2'),
                         ((), (('one',1),
                               ('two',2))))
        self.assertEqual(util.parse_args('one=1, two=2  ,three = hello'),
                         ((), (('one', 1),
                               ('two', 2),
                               ('three', 'hello'))))

    def test_positional_and_keyword_args(self):
        self.assertEqual(util.parse_args('1, two=2'),
                         ((1,), (('two',2),)))
        self.assertEqual(util.parse_args('1, three=hello'),
                         ((1,), (('three', 'hello'),)))

    def test_empty_args(self):
        self.assertEqual(util.parse_args(''), ((), ()))


class Test_pos2kwargs(unittest.TestCase):
    keys = ('one', 'two', 'three')

    def test_positional_args_only(self):
        self.assertEqual(util.pos2kwargs(posargs=(1,),
                                         kwargs=(),
                                         keys=self.keys),
                         (('one', 1),))
        self.assertEqual(util.pos2kwargs(posargs=(1, 2.5),
                                         kwargs=(),
                                         keys=self.keys),
                         (('one', 1), ('two', 2.5)))
        self.assertEqual(util.pos2kwargs(posargs=(1, 2.5, 'hello'),
                                         kwargs=(),
                                         keys=self.keys),
                         (('one', 1), ('two', 2.5), ('three', 'hello')))

    def test_keyword_args_only(self):
        self.assertEqual(util.pos2kwargs(posargs=(),
                                         kwargs=( ('one', 1), ),
                                         keys=self.keys),
                         (('one', 1),))
        self.assertEqual(util.pos2kwargs(posargs=(),
                                         kwargs=( ('one', 1), ('two', 2.5) ),
                                         keys=self.keys),
                         (('one', 1), ('two', 2.5)))
        self.assertEqual(util.pos2kwargs(posargs=(),
                                         kwargs=( ('one', 1), ('two', 2.5), ('three', 'hello') ),
                                         keys=self.keys),
                         (('one', 1), ('two', 2.5), ('three', 'hello')))

    def test_positional_and_keyword_args(self):
        self.assertEqual(util.pos2kwargs(posargs=(1,),
                                         kwargs=( ('two', 2.5), ),
                                         keys=self.keys),
                         (('one', 1), ('two', 2.5)))
        self.assertEqual(util.pos2kwargs(posargs=(1, 2.5),
                                         kwargs=( ('three', 'hello'), ),
                                         keys=self.keys),
                         (('one', 1), ('two', 2.5), ('three', 'hello')))
        self.assertEqual(util.pos2kwargs(posargs=(1,),
                                         kwargs=( ('three', 'hello'), ),
                                         keys=self.keys),
                         (('one', 1), ('three', 'hello')))

    def test_empty_args_with_keys(self):
        self.assertEqual(util.pos2kwargs(posargs=(), kwargs=(), keys=self.keys), ())

    def test_arg_given_twice(self):
        with self.assertRaisesRegex(error.Argument, r"'one' given twice: '1', 'one=11'"):
            util.pos2kwargs(posargs=('1',),
                            kwargs=( ('one', '11'), ),
                            keys=self.keys)
        with self.assertRaisesRegex(error.Argument, r"'two' given twice: '2', 'two=22'"):
            util.pos2kwargs(posargs=('1', '2'),
                            kwargs=( ('two', '22'), ),
                            keys=self.keys)

class Test_find_function_calls(unittest.TestCase):
    functions = ('f', 'f2')

    def test_one_function_without_id(self):
        text = 'some text f(argument) some more text'
        finder = util.find_function_calls(text, self.functions)
        self.assertEqual(next(finder), { 'func': 'f',
                                         'id': '',
                                         'argstr': 'argument',
                                         'len': len('f(argument)'),
                                         'start': len('some text '),
                                         'end': len('some text f(argument)') })
        with self.assertRaises(StopIteration):
            next(finder)
        self.assertIn(('f2', 'f'), util._FUNC_REGEXES)

    def test_one_function_with_id(self):
        text = 'some text f_x(argument) some more text'
        finder = util.find_function_calls(text, self.functions)
        self.assertEqual(next(finder), { 'func': 'f',
                                         'id': '_x',
                                         'argstr': 'argument',
                                         'len': len('f_x(argument)'),
                                         'start': len('some text '),
                                         'end': len('some text f_x(argument)') })
        with self.assertRaises(StopIteration):
            next(finder)

    def test_two_functions_without_id(self):
        text = 'a f(argument) b f2(a1, a2, a3=x)'
        finder = util.find_function_calls(text, self.functions)
        self.assertEqual(next(finder), { 'func': 'f',
                                         'id': '',
                                         'argstr': 'argument',
                                         'len': len('f(argument)'),
                                         'start': len('a '),
                                         'end': len('a f(argument)') })
        self.assertEqual(next(finder), { 'func': 'f2',
                                         'id': '',
                                         'argstr': 'a1, a2, a3=x',
                                         'len': len('f2(a1, a2, a3=x)'),
                                         'start': len('a f(argument) b '),
                                         'end': len('a f(argument) b f2(a1, a2, a3=x)') })
        with self.assertRaises(StopIteration):
            next(finder)

    def test_two_functions_with_id(self):
        text = 'a f_hello(argument) b f2_goodbye(a1, a2, a3=x)'
        finder = util.find_function_calls(text, self.functions)
        self.assertEqual(next(finder), { 'func': 'f',
                                         'id': '_hello',
                                         'argstr': 'argument',
                                         'len': len('f_hello(argument)'),
                                         'start': len('a '),
                                         'end': len('a f_hello(argument)') })
        self.assertEqual(next(finder), { 'func': 'f2',
                                         'id': '_goodbye',
                                         'argstr': 'a1, a2, a3=x',
                                         'len': len('f2_goodbye(a1, a2, a3=x)'),
                                         'start': len('a f_hello(argument) b '),
                                         'end': len('a f_hello(argument) b f2_goodbye(a1, a2, a3=x)') })
        with self.assertRaises(StopIteration):
            next(finder)

    def test_same_function_with_different_ids(self):
        text = 'a f_hello(argument) b f_goodbye(a1, a2, a3=x)'
        finder = util.find_function_calls(text, self.functions)
        self.assertEqual(next(finder), { 'func': 'f',
                                         'id': '_hello',
                                         'argstr': 'argument',
                                         'len': len('f_hello(argument)'),
                                         'start': len('a '),
                                         'end': len('a f_hello(argument)') })
        self.assertEqual(next(finder), { 'func': 'f',
                                         'id': '_goodbye',
                                         'argstr': 'a1, a2, a3=x',
                                         'len': len('f_goodbye(a1, a2, a3=x)'),
                                         'start': len('a f_hello(argument) b '),
                                         'end': len('a f_hello(argument) b f_goodbye(a1, a2, a3=x)') })
        with self.assertRaises(StopIteration):
            next(finder)

class Test_parse_function_calls(unittest.TestCase):
    functions = {
        'x': lambda how_many: 'x'*how_many,
        'y': lambda how_many: 'y'*how_many,
        'z': lambda how_many: 'z'*how_many,
    }

    def call_function(self, name, id, args):
        return self.functions[name](int(args))

    def test_replacement(self):
        text = util.parse_function_calls('bla x(3) more bla',
                                         tuple(self.functions.keys()),
                                         self.call_function)
        self.assertEqual(text, 'bla xxx more bla')

        text = util.parse_function_calls('bla x(5) more bla y(0)',
                                         tuple(self.functions.keys()),
                                         self.call_function)
        self.assertEqual(text, 'bla xxxxx more bla ')

        text = util.parse_function_calls('x(0)bla x(4) more bla y(2)z(1)',
                                         tuple(self.functions.keys()),
                                         self.call_function)
        self.assertEqual(text, 'bla xxxx more bla yyz')
