import unittest
from json import (loads, dumps)
import random
from i3barfodder import i3barproto as i3bp
from i3barfodder import error


class TestRGBColor(unittest.TestCase):
    def test_equality(self):
        self.assertEqual(i3bp.RGBColor('#123456'), i3bp.RGBColor('#123456'))
        self.assertEqual(i3bp.RGBColor('#123'), i3bp.RGBColor('#112233'))
        self.assertEqual(i3bp.RGBColor('#00ff00'), '#00ff00')
        self.assertEqual(i3bp.RGBColor('#00Ff33'), '#0F3')

    def test_combine(self):
        c1 = i3bp.RGBColor('#00ff00')
        c2 = i3bp.RGBColor('#0ff')
        combined = c1.combine(c2, 0.5)
        self.assertEqual(str(combined), '#00FF80')

    def test_invalid_value(self):
        with self.assertRaisesRegex(error.InvalidValue, r"Invalid RGB color: '#1234567'"):
            i3bp.RGBColor('#1234567')
        with self.assertRaisesRegex(error.InvalidValue, r"Invalid RGB color: '#1234'"):
            i3bp.RGBColor('#1234')
        with self.assertRaisesRegex(error.InvalidValue, r"Invalid RGB color: '#FFG'"):
            i3bp.RGBColor('#FFG')


class TestDynamicColor(unittest.TestCase):
    def setUp(self):
        self.dc = i3bp.DynamicColor()

    def test_no_color(self):
        self.assertEqual(str(self.dc), '')

    def test_single_color(self):
        self.dc.update(('colors', ('#905090',)), ('min',0), ('max',100), ('value',50))
        self.assertEqual(str(self.dc), '#905090')
        self.dc['value'] = 66
        self.assertEqual(str(self.dc), '#905090')

    def test_multiple_colors(self):
        self.dc['colors'] = ('#000000', '#ffffff')
        self.dc['min'] = 0
        self.dc['max'] = 100
        self.dc['value'] = 50
        self.assertEqual(str(self.dc), '#808080')

        self.dc['colors'] = '#ff0000-#00ff00-#0000ff'
        for value,color in ((-10, '#FF0000'), (100000, '#0000FF'), (25, '#808000')):
            self.dc['value'] = value
            self.assertEqual(str(self.dc), color)

        self.dc['colors'] = ['#0ff', '#f0f', '#ff0']
        self.dc['softmin'] = -1000
        self.dc['softmax'] = -900
        self.dc['scale'] = 'sqrt'
        self.dc['value'] = -975
        self.assertEqual(str(self.dc), '#FF00FF')
        self.dc['value'] = 1000
        self.assertEqual(str(self.dc), '#FFFF00')
        self.dc['scale'] = 'linear'
        self.dc['value'] = 0
        self.assertEqual(str(self.dc), '#FF00FF')
        self.dc['value'] = -1000
        self.assertEqual(str(self.dc), '#00FFFF')

        self.dc['colors'] = []
        self.assertEqual(str(self.dc), '')

    def test_invalid_values(self):
        with self.assertRaises(error.InvalidValue):
            self.dc['colors'] = '#12345'

        for color in ('#1234', '#1234567', '#1234gg', '#'):
            with self.assertRaises(error.InvalidValue):
                self.dc['colors'] = [color]

        with self.assertRaises(error.InvalidKey):
            self.dc['The Holy Grail']
        with self.assertRaises(error.InvalidKey):
            self.dc['Mickey'] = 'Mouse'

    def test_copy_and_equality(self):
        self.dc.update(('colors',('#123456', '#654321')),
                       ('value',5), ('min',-5e3), ('max',100e6), ('scale','log'))
        dc2 = self.dc.copy()
        self.assertEqual(str(self.dc), str(dc2))
        self.assertEqual(self.dc, dc2)

        new_colors = ('#000000', '#ffffff')
        dc2['colors'] = new_colors
        self.assertNotEqual(str(self.dc), str(dc2))
        self.assertNotEqual(self.dc, dc2)

        dc2['min'] = 0
        dc2['scale'] = 'linear'
        self.dc.update(('colors',new_colors), ('min',0), ('scale','linear'))
        self.assertEqual(str(self.dc), str(dc2))
        self.assertEqual(self.dc, dc2)

    def test_string_parser(self):
        args = self.dc.parse('value=251:colors=#888-#fff:softmax=501')
        self.assertEqual(args, (('value',251), ('colors','#888-#fff'), ('softmax',501)))

        args = self.dc.parse('value=251:#888-#fff:softmax=501')
        self.assertEqual(args, (('colors','#888-#fff'), ('value',251), ('softmax',501)))

        args = self.dc.parse('251:colors=#888-#fff:softmax=501')
        self.assertEqual(args, (('value',251), ('colors','#888-#fff'), ('softmax',501)))

        args = self.dc.parse('251:#888-#fff:softmax=501')
        self.assertEqual(args, (('value',251), ('colors','#888-#fff'), ('softmax',501)))

        args = self.dc.parse('#888-#fff:251:softmax=501')
        self.assertEqual(args, (('value',251), ('colors','#888-#fff'), ('softmax',501)))

        args = self.dc.parse('251:#888-#fff:501')
        self.assertEqual(args, (('value',251), ('colors','#888-#fff'), ('min',501)))

        args = self.dc.parse('#888-#fff:251:501')
        self.assertEqual(args, (('value',251), ('colors','#888-#fff'), ('min',501)))

        args = self.dc.parse('#888-#fff:251:501:scale=log:softmax=600')
        self.assertEqual(args, (('value',251), ('colors','#888-#fff'), ('min',501),
                                ('scale','log'), ('softmax',600)))

        args = self.dc.parse('251:#888-#fff:501:700:401:800:log')
        self.assertEqual(args, (('value',251), ('colors','#888-#fff'), ('min',501), ('max',700),
                                ('softmin',401), ('softmax',800), ('scale','log')))




class TestI3Block(unittest.TestCase):
    def test_defaults(self):
       b = i3bp.I3Block()
       self.assertTrue('full_text' in b)

    def setUp(self):
        self.b = {
            'full_text': 'Full text test',
            'short_text': 'Short text test',
            'align': 'center',
            'instance': '?',
            'markup': 'none',
            'min_width': 300,
            'name': 'manually set',
            'separator': False,
            'separator_block_width': 42,
            'urgent': True,
            'color': '#123456',
        }

        self.dc = {
            'dyncolor_colors': ['#ff0000','#00ff00','#0000ff'],
            'dyncolor_min': 0,
            'dyncolor_max': 100,
            'dyncolor_value': 50,
            'dyncolor_scale': 'linear',
        }

    def test_valid_values(self):
        b = i3bp.I3Block(**self.b)
        for key,val in self.b.items():
            self.assertEqual(b[key], self.b[key])

        for key,val in ( ('align', 'left'), ('markup', 'pango'),
                         ('min_width', 0), ('separator', True),
                         ('separator_block_width', 9),
                         ('urgent', False) ):
            if random.choice((True, False)):
                b[key] = val
            else:
                b.update(**{key: val})
            self.assertEqual(b[key], val)
        b['color'] = '#fff'
        self.assertEqual(b['color'], '#FFFFFF')

    def test_invalid_values(self):
        b = i3bp.I3Block()

        b.update(dyncolor_value=3, color='#123')
        self.assertIn('ambiguous', b.errors[0])

        for key,val,errparts in ( ('align', 'yellow', ('must be', 'left', 'right', 'center')),
                                  ('markup', 7, ('must be', 'pango', 'none')),
                                  ('min_width', 'Nathan', ('not a number',)),
                                  ('separator', 'monkey', ('must be', 'true', 'false')),
                                  ('separator_block_width', 'yes', ('not a number',)),
                                  ('urgent', 'you bet!', ('must be', 'on', 'off', 'yes', 'no')),
                                  ('color', range(50), ('not a number',)) ):
            if random.choice((True, False)):
                b[key] = val
            else:
                b.update(**{key: val})
            errs = b.errors
            self.assertEqual(len(errs), 1)
            err = errs[0].lower()
            self.assertIn(str(val).lower(), err)
            for errpart in errparts:
                self.assertIn(errpart.lower(), err)

    def test_dynamic_color_with_linear_scale(self):
        b = i3bp.I3Block(**self.dc)
        self.assertEqual(b['color'], '#00FF00')
        for val,col in ((25, '#808000'), (75, '#008080'), (100, '#0000FF')):
            b['dyncolor_value'] = val
            self.assertEqual(b['color'], col)

    def test_dynamic_color_with_log_scale(self):
        b = i3bp.I3Block(**self.dc)
        b['dyncolor_scale'] = 'log'
        for val,col in ((10, '#00FF00'), (3.17, '#808000'), (31.7, '#008080')):
            b['dyncolor_value'] = val
            self.assertEqual(b['color'], col)

    def test_switching_between_dynamic_and_fixed_color(self):
        b = i3bp.I3Block(**self.b)
        b['color'] = '#00FF00'
        self.assertEqual(b['color'], '#00FF00')

        # Fixed color is used until dyncolor_colors is set
        b['dyncolor_softmin'] = -50
        b['dyncolor_max'] = 50
        self.assertEqual(b['color'], '#00FF00')
        b['dyncolor_value'] = 0
        self.assertEqual(b['color'], '#00FF00')
        b['dyncolor_colors'] = ['#000000', '#ffffff']
        self.assertEqual(b['color'], '#808080')  # Mixing works now
        for val,col in ( (-50, '#000000'), (-25, '#404040'),
                         (25, '#C0C0C0'), (50, '#FFFFFF') ):
            b['dyncolor_value'] = val
            self.assertEqual(b['color'], col)

        # Setting a fixed color disables dynamic color
        b['color'] = '#f00'
        self.assertEqual(b['color'], '#FF0000')

        # Setting any 'dynamic_*' field re-enables dynamic color
        b['dyncolor_softmax'] = 150
        b['dyncolor_value'] = 100
        self.assertEqual(b['color'], '#C0C0C0')
        b['dyncolor_value'] = 250
        self.assertEqual(b['color'], '#FFFFFF')
        b['dyncolor_value'] = 100
        self.assertEqual(b['color'], '#808080')
        b.update(color='#123')
        self.assertEqual(b['color'], '#112233')
        b.update(dyncolor_value=-1000)
        self.assertEqual(b['color'], '#000000')
        b.update(dyncolor_max=1000, dyncolor_value=0)
        self.assertEqual(b['color'], '#808080')

    def test_dynamic_color_parser(self):
        b = i3bp.I3Block(color='#fff')
        self.assertEqual(b['color'], '#FFFFFF')

        b['color'] = '50:#000-#ffffff:max=10:min=0'
        self.assertEqual(b['color'], '#FFFFFF')
        for val,col in ((0, '#000000'), (5, '#808080'), (10, '#FFFFFF')):
            b['color'] = val
            self.assertEqual(b['color'], col)

        for val,col in ((0, '#000000'), (5, '#808080'), (10, '#FFFFFF')):
            b['dyncolor_value'] = val
            self.assertEqual(b['color'], col)

        b['color'] = '#123456'
        self.assertEqual(b['color'], '#123456')

        b['color'] = 'scale=sqrt:softmax=100'
        for val,col in (('value=25', '#808080'), (1000, '#FFFFFF'), ('250', '#808080')):
            b['color'] = val

    def test_vertical_bar(self):
        b = i3bp.I3Block(full_text='vbar(min=-200, softmax=-120, value=-120)')
        fields = loads(b.json)
        self.assertEqual(fields['full_text'], '\u2588')

        for value,char in ((-190,'\u2581'), (-180,'\u2582'), (-170,'\u2583'), (-160,'\u2584'),
                           (-150,'\u2585'), (-140,'\u2586'), (-130,'\u2587'), (-120,'\u2588')):
            b['full_text'] = 'vbar({})'.format(value)
            fields = loads(b.json)
            self.assertEqual(fields['full_text'], char)

        b.update(full_text='vbar(min=0, softmax=10, scale=sqrt, value=2.5)')
        fields = loads(b.json)
        self.assertEqual(fields['full_text'], '\u2584')

    def test_invalid_vbar_calls(self):
        b = i3bp.I3Block(full_text='vbar(value=much)')
        error = b.errors[0]
        self.assertIn("Invalid vbar argument", error)
        self.assertIn("not a number", error)
        self.assertEqual(len(b.errors), 0)
        b['full_text'] = 'vbar(horizontal=yes)'
        self.assertEqual(b.errors[0], "Invalid vbar argument: 'horizontal'")

    def test_multiple_vertical_bars(self):
        b = i3bp.I3Block()
        bars = {
            'one': (1000, 2000, 'linear', 1500),
            'two': (-100, 0, 'sqrt', -75),
            'three': (0, 100, 'log', 10),
        }
        full_text = []
        for name,(min,max,scale,val) in sorted(bars.items()):
            full_text.append('{0}:vbar_{0}(value={4}, min={1}, max={2}, scale={3})'
                             .format(name, min, max, scale, val))
        b['full_text'] = ' -- '.join(full_text)
        fields = loads(b.json)
        self.assertEqual(fields['full_text'], ' -- '.join(name+':\u2584' for name in sorted(bars)))

        b['full_text'] = 'vbar_one(min={}, max={}, value=-1000)'.format(10e3, 100e3)
        fields = loads(b.json)
        self.assertEqual(fields['full_text'], ' ')

        b['full_text'] = 'vbar_one(50k) vbar_two(-50, scale=linear)'
        fields = loads(b.json)
        self.assertEqual(fields['full_text'], '\u2584 \u2584')

        b['full_text'] = 'vbar_two(-90)'
        fields = loads(b.json)
        self.assertEqual(fields['full_text'], '\u2581')

        b['full_text'] = 'three:vbar_three(50, scale=linear) -- one:vbar_one(22%100k)'
        fields = loads(b.json)
        self.assertEqual(fields['full_text'], 'three:\u2584 -- one:\u2581')

    def test_equality(self):
        fields1 = {'separator': True}
        fields2 = self.dc.copy()
        b1 = i3bp.I3Block(**fields1)
        b2 = i3bp.I3Block(**fields2)
        self.assertNotEqual(b1, b2)
        b1.update(**fields2)
        self.assertNotEqual(b1, b2)
        b2.update(**fields1)
        self.assertEqual(b1, b2)

    def test_copy(self):
        b1 = i3bp.I3Block(**self.b)
        b2 = b1.copy()
        print(b1)
        print(b2)

        self.assertEqual(b1, b2)
        b1['short_text'] = 'different'
        self.assertNotEqual(b1, b2)
        b2['short_text'] = 'different'
        self.assertEqual(b1, b2)
        b2['dyncolor_colors'] = ['invalid', 'colors']
        self.assertEqual(b1, b2)
        b2['dyncolor_colors'] = ['#000', '#ffffff']
        self.assertNotEqual(b1, b2)
