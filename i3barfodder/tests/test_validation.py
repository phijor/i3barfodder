import unittest
import os
from i3barfodder import (validation, error)

class TestVDict(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.validators = {
            'int,max=50': lambda v: validation.validate_integer(v, max=50),
            'num,min=-10': lambda v: validation.validate_number(v, min=-10),
            'num,50-100': lambda v: validation.validate_number(v, min=50, max=100),
            'silent': lambda v: validation.validate_integer(v, min=10, max=20, silent=True),
            'bool': validation.validate_bool,
            'opts': lambda v: validation.validate_option(v, options=('red','green','blue')),
            'lst': validation.validate_list,
            'path': validation.validate_path,
            'PATH': validation.validate_PATH,
        }

    def test_defaults(self):
        defaults = { 'num,min=-10': 0.1, 'int,max=50': -50,
                   'bool': 'yes', 'opts': 'green', 'lst': (1,2,3),
                   'path': '~/some/path',
                   'PATH': '/some/test/path' }
        vd = validation.VDict(validators=type(self).validators, **defaults)
        self.assertEqual(vd['num,min=-10'], 0.1)
        self.assertEqual(vd['int,max=50'], -50)
        with self.assertRaises(KeyError):
            vd['num,50-100']  # Not specified in defaults
        vd['num,50-100'] = 75
        self.assertEqual(vd['num,50-100'], 75)
        self.assertEqual(vd['bool'], True)
        self.assertEqual(vd['opts'], 'green')
        self.assertEqual(vd['lst'], [1,2,3])
        self.assertEqual(vd['path'], os.environ['HOME']+'/some/path')
        expected_path = os.pathsep.join([os.environ['PATH'], '/some/test/path'])
        self.assertEqual(vd['PATH'], expected_path)

    def test_restricted_keys(self):
        vd = validation.VDict(validators=type(self).validators,
                              valid_keys=('bool', 'lst'))
        # 'bool' and 'lst' can be set, but nothing else
        vd['bool'] = 'no'
        vd['lst'] = ('one', 'two', 'three')
        with self.assertRaises(error.InvalidKey):
            vd['num,50-100'] = 70

    def test_unrestricted_keys(self):
        vd = validation.VDict(validators=type(self).validators)
        vd['unknown key'] = 'unvalidated value'
        self.assertEqual(vd['unknown key'], 'unvalidated value')

    def test_number_limits(self):
        vd = validation.VDict(validators=type(self).validators)

        with self.assertRaisesRegex(error.InvalidValue, r'too small'):
            vd['num,min=-10'] = -11
        with self.assertRaisesRegex(error.InvalidValue, r'too small'):
            vd['num,min=-10'] = -10.0001
        vd['num,min=-10'] = -10
        self.assertEqual(vd['num,min=-10'], -10)
        vd['num,min=-10'] = 1e42
        self.assertEqual(vd['num,min=-10'], 1e42)
        vd['num,min=-10'] = 'inf'
        self.assertEqual(vd['num,min=-10'], float('inf'))

        with self.assertRaisesRegex(error.InvalidValue, r'too big'):
            vd['int,max=50'] = 51
        with self.assertRaisesRegex(error.InvalidValue, r'not an integer'):
            vd['int,max=50'] = 2.5
        vd['int,max=50'] = -1000
        self.assertEqual(vd['int,max=50'], -1000)

        with self.assertRaisesRegex(error.InvalidValue, r'too small'):
            vd['num,50-100'] = 49
        with self.assertRaisesRegex(error.InvalidValue, r'too big'):
            vd['num,50-100'] = 101
        vd['num,50-100'] = 50.123456789
        self.assertEqual(vd['num,50-100'], 50.123456789)
        vd['num,50-100'] = 100
        self.assertEqual(vd['num,50-100'], 100.0)

    def test_number_unit_prefixes(self):
        vd = validation.VDict(validators={
            'number': lambda v: validation.validate_number(v, max='1000T', min='-100Ti'),
        })
        for val,exp in ( ('10', 10),

                         ('10k', 10*1000), ('10 M', 10*1000**2),
                         ('10 G', 10*1000**3), ('10T', 10*1000**4),

                         ('10 Ki', 10*1024), ('10Mi', 10*1024**2),
                         ('10 Gi', 10*1024**3), ('10 Ti', 10*1024**4),

                         ('10.12345 k', 10123.45), ('10.123 k', 10123) ):
            vd['number'] = val
            self.assertEqual(vd['number'], exp)
            self.assertEqual(type(vd['number']), type(exp))

        with self.assertRaisesRegex(error.InvalidValue, r'too big'):
            vd['number'] = '1000.1T' # Too big
        with self.assertRaisesRegex(error.InvalidValue, r'too small'):
            vd['number'] = '-100.1Ti' # Too small

    def test_percentage(self):
        vd = validation.VDict(validators={
            'x': lambda v: validation.validate_number(v),
        })
        vd['x'] = '10%37'
        self.assertEqual(vd['x'], 3.7)
        vd['x'] = '1%2.5'
        self.assertEqual(vd['x'], 0.025)
        vd['x'] = '200%5k'
        self.assertEqual(vd['x'], 10000)

    def test_options(self):
        vd = validation.VDict(validators=type(self).validators)
        with self.assertRaisesRegex(error.InvalidValue, r'must be one of'):
            vd['opts'] = 'salty'
        vd['opts'] = 'blue'
        self.assertEqual(vd['opts'], 'blue')

    def test_PATH(self):
        vd = validation.VDict(validators=type(self).validators)
        vd['PATH'] = '/path/to/stuff/'
        actual_PATH = os.pathsep.join([os.environ['PATH'], '/path/to/stuff/'])
        self.assertEqual(vd['PATH'], actual_PATH)

    def test_silent_limits(self):
        vd = validation.VDict(validators=type(self).validators)
        vd['silent'] = 0
        self.assertEqual(vd['silent'], 10)
        vd['silent'] = 1000
        self.assertEqual(vd['silent'], 20)
        vd['silent'] = 15.8
        self.assertEqual(vd['silent'], 16)

    def test_update(self):
        vd = validation.VDict(validators=type(self).validators,
                            bool=False, opts='red')
        with self.assertRaises(KeyError):
            vd['lst']

        vd.update(lst=['some', 'thing'])
        self.assertEqual(vd['lst'], ['some', 'thing'])

        vd.update({'lst': ['any', 'thing']})
        self.assertEqual(vd['lst'], ['any', 'thing'])
