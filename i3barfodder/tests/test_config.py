import unittest
from i3barfodder import (config, error)


class TestConfigParser(unittest.TestCase):
    def test_settings_without_section(self):
        dct = config.ConfigParser('''
            21 = [one, two, three]
            some=thing
            thong =  <- one space -- two spaces ->  
            blah      = this is some string (or is it?)
        ''')
        self.assertEqual(dct[None]['21'], '[one, two, three]')
        self.assertEqual(dct[None]['some'], 'thing')
        self.assertEqual(dct[None]['thong'], ' <- one space -- two spaces ->  ')
        self.assertEqual(dct[None]['blah'], 'this is some string (or is it?)')

    def test_settings_with_sections(self):
        dct = config.ConfigParser('''
            general = setting

            [section1]
            some=thing
            thong =  <- one space -- two spaces ->  
            blah      = this is some string (or is it?)

            [section2]
                some=else
            hello = goodbye!
            yes=no
        ''')

        self.assertEqual(str(dct), ('general = setting\n'
                                    '\n'
                                    '[section1]\n'
                                    'blah = this is some string (or is it?)\n'
                                    'some = thing\n'
                                    'thong =  <- one space -- two spaces ->  \n'
                                    '\n'
                                    '[section2]\n'
                                    'hello = goodbye!\n'
                                    'some = else\n'
                                    'yes = no\n\n'))

        self.assertEqual(dct[None]['general'], 'setting')

        self.assertEqual(dct['section1']['some'], 'thing')
        self.assertEqual(dct['section1']['thong'], ' <- one space -- two spaces ->  ')
        self.assertEqual(dct['section1']['blah'], 'this is some string (or is it?)')

        self.assertEqual(dct['section2']['some'], 'else')
        self.assertEqual(dct['section2']['hello'], 'goodbye!')
        self.assertEqual(dct['section2']['yes'], 'no')

    def test_comments(self):
        dct = config.ConfigParser('''
            # Should be 1, ideally.
            one = 2
            # But who cares, this is just a test.

            [section]
                bla=bli
        ''')
        self.assertEqual(dct[None]['one'], '2')
        self.assertEqual(dct['section']['bla'], 'bli')

    def test_errors(self):
        with self.assertRaisesRegex(error.ConfigParser,
                                    r"Invalid section name: 'b\[la\]'"):
            config.ConfigParser('b[la]')

        with self.assertRaisesRegex(error.ConfigParser,
                                    "Invalid setting: 'no_value'"):
            config.ConfigParser('no_value')

        with self.assertRaisesRegex(error.ConfigParser,
                                    "Invalid setting in section 'test': 'bla'"):
            config.ConfigParser('[test]\nbla')


class TestConfig(unittest.TestCase):
    def test_global_variables(self):
        cfg = config.Config('show_updates = yes\ndelay=1')
        self.assertEqual(cfg['show_updates'], True)
        self.assertEqual(cfg['delay'], 1)
        self.assertEqual(cfg.sections, {})
        self.assertEqual(cfg.order, ())

    def test_errors(self):
        with self.assertRaisesRegex(error.Config, r'No such file'):
            config.Config('/tmp/ifthisfileexiststhistestfails')

        with self.assertRaisesRegex(error.Config, r'Nonsensical config object'):
            config.Config([1, 2, 3])

        with self.assertRaisesRegex(error.Config, r"Invalid 'show_updates' value"):
            config.Config('show_updates = maybe?\n')

        with self.assertRaisesRegex(error.Config, r"Invalid 'show_updates' value"):
            config.Config('show_updates = maybe?\n')

    def test_sections(self):
        cfg = config.Config('''
            stuff = whatever
            thingy = woob

            [section1]
            command = does not compute
            color = #abcdef

            [section2]
            command = computes like a charm
            color = #1234567
            thingy = thongy

            [section_empty]''')

        expected_sections = ('section1', 'section2', 'section_empty')
        self.assertTupleEqual(cfg.order, expected_sections)
        for es in expected_sections:
            self.assertIsInstance(cfg.sections[es], dict)

        self.assertListEqual(sorted(cfg.sections['section1'].keys()),
                             ['color', 'command', 'name', 'stuff', 'thingy'])
        self.assertEqual(cfg.sections['section1']['name'], 'section1')
        self.assertEqual(cfg.sections['section1']['stuff'], 'whatever')
        self.assertEqual(cfg.sections['section1']['thingy'], 'woob')
        self.assertEqual(cfg.sections['section1']['command'], 'does not compute')
        self.assertEqual(cfg.sections['section1']['color'], '#abcdef')

        self.assertListEqual(sorted(cfg.sections['section2'].keys()),
                             ['color', 'command', 'name', 'stuff', 'thingy'])
        self.assertEqual(cfg.sections['section2']['name'], 'section2')
        self.assertEqual(cfg.sections['section2']['stuff'], 'whatever')
        self.assertEqual(cfg.sections['section2']['thingy'], 'thongy')

        self.assertEqual(sorted(cfg.sections['section_empty'].keys()),
                         ['name', 'stuff', 'thingy'])
        self.assertEqual(cfg.sections['section_empty']['name'], 'section_empty')
        self.assertEqual(cfg.sections['section_empty']['stuff'], 'whatever')
        self.assertEqual(cfg.sections['section_empty']['thingy'], 'woob')

